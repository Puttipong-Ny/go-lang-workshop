package service

import (
	"encoding/json"
	"server/logs"
	"server/repository"
	"time"

	"github.com/go-redis/redis"
)

type customerServiceRedis struct {
	custRepo    repository.CustomerRepository
	redisClient *redis.Client
}

func NewCustomerServiceRedis(custRepo repository.CustomerRepository, redisClient *redis.Client) CustomerService {
	return customerServiceRedis{custRepo, redisClient}
}

func (s customerServiceRedis) GetCustomers() (customers []CustomerResponse, err error) {

	key := "service::GetCustomers"

	//Redis GET
	if customersJson, err := s.redisClient.Get(key).Result(); err == nil {
		if json.Unmarshal([]byte(customersJson), &customers) == nil {
			logs.Info("redis")
			return customers, nil
		}
	}

	// Repository
	customersDB, err := s.custRepo.GetCustomers()
	if err != nil {
		return nil, err
	}

	for _, customer := range customersDB {
		customers = append(customers, CustomerResponse{
			CustomerID:   customer.CustomerID,
			CustomerName: customer.CustomerName,
			PhoneNumber:  customer.PhoneNumber,
			BirthDate:    customer.BirthDate,
		})
	}

	// Redis SET
	if data, err := json.Marshal(customers); err == nil {
		s.redisClient.Set(key, string(data), time.Second*10)
	}
	logs.Info("database")
	return customers, nil
}
