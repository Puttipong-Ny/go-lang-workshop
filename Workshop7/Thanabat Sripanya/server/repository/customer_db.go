package repository

import (
	"fmt"
	"log"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type customerRepositoryDB struct {
	db *gorm.DB
}

func NewCustomerRepositoryDB(db *gorm.DB) CustomerRepository {
	db.AutoMigrate(&Customer{})
	mockData(db)
	return customerRepositoryDB{db}
}

func (r customerRepositoryDB) GetCustomers() ([]Customer, error) {
	time.Sleep(time.Microsecond * 1000)
	customers := []Customer{}
	result := r.db.Preload(clause.Associations).Find(&customers).Limit(30)
	fmt.Printf("%v/n", customers)
	if result.Error != nil {
		log.Printf("Error getting all customers: %v", result.Error)
		return nil, result.Error
	}
	log.Printf("Retrieved all customers (%d total)", len(customers))
	return customers, nil
}

