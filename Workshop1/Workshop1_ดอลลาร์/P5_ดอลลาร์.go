package main

import "fmt"

func fibonacci(nterms int) {
	n1 := 0
	n2 := 1
	count := 0
	fmt.Println("Fibonacci sequence:")
	fmt.Println(n1)
	for count < nterms-1 {
		nth := n1 + n2
		n1 = n2
		n2 = nth
		fmt.Println(nth)
		count++
	}
}

func main() {
	fibonacci(10)
}
