package main

import "fmt"

type Product struct {
	ProductName string
	Price       float32
	Weight      float32
}

func main() {
	s := Product{
		ProductName: "iPhone 13",
		Price:       26900,
		Weight:      0.12,
	}
	fmt.Println("Product name:", s.ProductName)
	fmt.Println("Price:", s.Price)
	fmt.Println("Weight:", s.Weight, "kg")

}
