package main

import "fmt"

var count int

func main() {
    fmt.Scanf("%d",&count)
    fabo:= make([]int, count)
    for i := 0; i < count ; i++ {
        if i < 2 {
            fabo[i] = i
        } else {
        	fabo[i] = fabo[i-1] + fabo[i-2]
        }
    }

    
    fmt.Println(fabo)
}