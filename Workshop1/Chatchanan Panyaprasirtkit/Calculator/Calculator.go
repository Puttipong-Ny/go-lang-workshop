package main

import (
	"fmt"
	"strconv"
	"workspace/P4"
)

var numList []float64
var operatorList []string
var result float64

func main() {
	userInput, _ := strconv.ParseInt(P4.Input("How many number:"), 10, 64)
	for i := 1; i <= int(userInput); i++ {
		numInput, _ := strconv.ParseFloat(P4.Input("Number"+strconv.Itoa(i)+":"), 64)
		numList = append(numList, numInput)
		if i < int(userInput) {
			operatorList = append(operatorList, P4.Input("Operator :"))
		}
	}
	result = numList[0]
	for i := 0; i < len(operatorList); i++ {
		switch operatorList[i] {
		case "+":
			result = numList[i+1] + result
		case "-":
			result = numList[i+1] - result
		case "*":
			result = numList[i+1] * result
		case "/":
			result = numList[i+1] / result
		}
	}
	fmt.Println("result:", result)
}
