package main

import (
	"fmt"
	"os"
)

func main() {
	var num1, num2 int

	fmt.Print("Enter the first number: ")
	fmt.Scan(&num1)

	fmt.Print("Enter the second number (can't be zero): ")
	fmt.Scan(&num2)

	if num2 == 0 {
		fmt.Println("Error: Second parameter must be a non-zero integer.")
		os.Exit(0)
	}

	addition := num1 + num2
	subtraction := num1 - num2
	multiplication := num1 * num2
	division := float64(num1) / float64(num2)

	// Display the results to the user
	fmt.Println("Addition:", addition)
	fmt.Println("Subtraction:", subtraction)
	fmt.Println("Multiplication:", multiplication)
	fmt.Println("Division:", division)
}
