package main

import "fmt"

// var numbers = []float32{1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5}
type Product struct {
	name   string
	price  int
	weight float32
}

func main() {
	var product Product
	listProduct := []Product{}

	product.name = "iPhone 13"
	product.price = 26900
	product.weight = 0.21

	listProduct = append(listProduct, product)

	//fmt.Println(listProduct)
	fmt.Println("Product name : ", product.name)
	fmt.Println("Price : ", product.price)
	fmt.Println("Weight : ", product.weight, "kg")

}

// count := len(numbers)
// fmt.Printf("Count of array is %d\n", count)
// var result float32 = 0
// for i := 0; i < count; i++ {
// 	result += numbers[i]
// 	result = result + numbers[i]

// }
// sum := result / float32(count)
// fmt.Printf("Average result is %.2f", sum)
