package service

import (
	"bank/errs"
	"bank/logs"
	"bank/repository"
	"database/sql"
	"log"
	"time"
)

type productService struct {
	proRepo repository.ProductRepository
}

func NewProductService(proRepo repository.ProductRepository) productService {
	return productService{proRepo: proRepo}
}

func (s productService) GetProducts() ([]ProductResponse, error) {

	products, err := s.proRepo.GetAll()
	if err != nil {
		log.Println(err)
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	custResponses := []ProductResponse{}
	for _, product := range products {
		custResponse := ProductResponse{
			ProductId:     product.ProductId,
			Name:          product.Name,
			Price:         product.Price,
			ProductDetail: product.ProductDetail,
			DateCreate:    time.Now().Format("2006-1-2 15:04:05"),
		}
		custResponses = append(custResponses, custResponse)
	}
	return custResponses, nil
}

func (s productService) GetProduct(id int) (*ProductResponse, error) {
	product, err := s.proRepo.GetById(id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errs.NewNotFoundError("Product not found")
		}
		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}

	custResponse := ProductResponse{
		ProductId:     product.ProductId,
		Name:          product.Name,
		Price:         product.Price,
		ProductDetail: product.ProductDetail,
		DateCreate:    product.DateCreate,
	}
	return &custResponse, nil
}

func (s productService) AddProduct(product ProductResponse) (int, error) {
	newProduct := repository.Product{
		Name:          product.Name,
		Price:         product.Price,
		ProductDetail: product.ProductDetail,
		DateCreate:    time.Now().Format("2006-1-2 15:04:05"),
	}
	err := s.proRepo.Insert(newProduct)
	if err != nil {
		logs.Error(err)
		return 0, errs.NewUnexpectedError()
	}

	return newProduct.ProductId, nil
}

func (s productService) UpdateProduct(id int, req ProductResponse) error {
	prod := repository.Product{
		ProductId:     id,
		Name:          req.Name,
		Price:         req.Price,
		ProductDetail: req.ProductDetail,
		DateCreate:    time.Now().Format("2006-1-2 15:04:05"),
	}

	err := s.proRepo.Update(id, prod)
	if err != nil {
		logs.Error(err)
		return errs.NewUnexpectedError()
	}

	return nil
}

func (s productService) DeleteProduct(productId int) error {
	err := s.proRepo.Delete(productId)
	if err != nil {
		logs.Error(err)
		return errs.NewUnexpectedError()
	}
	return nil
}
