package logs

import (
	"go.uber.org/zap"
)

// var Log *zap.Logger
var log *zap.Logger

func init() {
	config := zap.NewProductionConfig()
	//config.EncoderConfig.TimeKey = "timestamp"
	//config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	config.EncoderConfig.StacktraceKey = ""

	//Log, _ = config.Build()
	var err error
	log, err = config.Build(zap.AddCallerSkip(1))
	if err != nil {
		panic(err)
	}

}

func Info(message string, fields ...zap.Field) {
	log.Info(message, fields...)
}

func Debug(message string, fields ...zap.Field) {
	log.Debug(message, fields...)
}

func Error(message interface{}, fields ...zap.Field) { // ถ้ามันเป็น {} มันเป็นอ็อปเจ็ค ในที่นี้คือ interface{}
	switch v := message.(type) {
	case error:
		log.Error(v.Error(), fields...)
	case string: // ถ้าส่งมาเป็น string ซึ่ง v เป็น string อยู่แล้ว
		log.Error(v, fields...)
	}
}
