package service

type CustomerResponse struct {
	CustomerID   int    `json:"Customer_id"`
	CustomerName string `json:"Customer_name"`
	PhoneNumber  string `json:"Phone_number"`
	DateCreated  string `json:"Date_created"`
}

type CustomerService interface {
	GetCustomers() ([]CustomerResponse, error)
	GetCustomer(int) (*CustomerResponse, error)
	InsertCustomer(CustomerResponse) (int, error)
	RemoveCustomer(int) error
	UpdateCustomer(CustomerResponse) (int, error)
}

