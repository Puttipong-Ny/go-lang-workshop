package repository

import (
	"github.com/jmoiron/sqlx"
)

type CustomerRepositoryDB struct {
	db *sqlx.DB
}

func NewCustomerRepositoryDB(db *sqlx.DB) CustomerRepositoryDB {
	return CustomerRepositoryDB{db: db}
}
func (r CustomerRepositoryDB) GetAll() ([]Customer, error) {
	customers := []Customer{}
	query := "select customer_id, name, date_create, phone_number from customers"
	err := r.db.Select(&customers, query)
	if err != nil {
		return nil, err
	}
	return customers, nil
}
func (r CustomerRepositoryDB) GetById(id int) (*Customer, error) {
	customer := &Customer{}
	query := "select customer_id, name, date_create, phone_number from customers where customer_id = $1"
	err := r.db.Get(customer, query, id)
	if err != nil {
		return nil, err
	}
	return customer, nil
}
func (r CustomerRepositoryDB) DeleteCustomer(id int) error {
	query := "DELETE FROM customers WHERE customer_id = $1"
	_, err := r.db.Exec(query, id)
	if err != nil {
		return nil
	}
	return err
}
func (r CustomerRepositoryDB) InsertCustomer(c Customer) (*Customer, error) {
	query := "INSERT INTO customers (customer_id, name, date_create, phone_number) VALUES ($1, $2, $3, $4)"
	_, err := r.db.Exec(
		query,
		c.CustomerID,
		c.Name,
		c.DateCreate,
		c.PhoneNumber,
	)
	if err != nil {
		return nil, err
	}
	return &c, nil
}
func (r CustomerRepositoryDB) UpdateCustomer(id int, c Customer) (*Customer, error) {
	query := "UPDATE customers SET name=$1, date_create=$2, phone_number=$3 WHERE customer_id=$4"
	_, err := r.db.Exec(query, c.Name, c.DateCreate, c.PhoneNumber, id)
	if err != nil {
		return nil, err
	}
	updatedCustomer, err := r.GetById(id)
	if err != nil {
		return nil, err
	}
	return updatedCustomer, nil
}




