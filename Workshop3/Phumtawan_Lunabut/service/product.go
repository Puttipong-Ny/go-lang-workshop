package service

type NewProductRequest struct {
	Product_Name	string	`json:"product_name"`
	Price			float64	`json:"price"`
	Product_Detail	string	`json:"product_detail"`
}

type ProductResponse struct {
	ID				int		`json:"id"`
	Product_Name	string	`json:"product_name"`
	Price			float64	`json:"price"`
	Product_Detail	string	`json:"product_detail"`
	Date_Created	string	`json:"date_created"`
}

type ProductService interface {
	NewProduct(int, NewProductRequest)(*ProductResponse, error)
	GetProducts(int) (*ProductResponse, error)
	GetAllProducts()([]ProductResponse, error)
	UpdateProduct(int, NewProductRequest) (*ProductResponse, error)
	DeleteProduct(int) (error)
}