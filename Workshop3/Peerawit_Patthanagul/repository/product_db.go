package repository

import (

	//"github.com/Peerawitptk/go4web/repository"
	//"context"
	//"fmt"
	//"time"

	//"log"

	//"github.com/Peerawitptk/go4web/errs"
	"context"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// ตัวใหญ่จะให้ package ตัวอื่นใช้ได้ ตัวเล็กจะเป็น Private ตัวอักษรตัวแรก
type ProductRepositoryDB struct {
	db *sqlx.DB
}

func NewProductRepositoryDB(db *sqlx.DB) ProductRepositoryDB {
	return ProductRepositoryDB{db: db}
}

func (r ProductRepositoryDB) GetAll() ([]Product, error) {
	products := []Product{}
	query := "select id, product_name, price, product_detail, date_created from product"
	err := r.db.Select(&products, query)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (r ProductRepositoryDB) GetById(id int) (*Product, error) {
	product := Product{}
	query := "select id, product_name, price, product_detail, date_created from product where id = ?"
	err := r.db.Get(&product, query, id)
	if err != nil{
		return nil, err
	}
	return &product, nil
}


func (r ProductRepositoryDB) GetPost(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "INSERT INTO product (id, product_name, price, product_detail ,date_created) VALUES (?, ?, ?,?,?)"
	result, err := r.db.ExecContext(ctx, query, product.ID, product.Product_Name, product.Price,product.Product_Detail,time.Now())
	if err != nil {
		return 0, err
	}
	lastInsertID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(lastInsertID), nil
}


func (r ProductRepositoryDB) GetDeleteById(ID int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "DELETE FROM product WHERE id = ?"
	_, err := r.db.ExecContext(ctx, query, ID)
	if err != nil {
		return err
	}
	return nil
}


func (r ProductRepositoryDB) GetUpdate(product Product) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	query := "UPDATE product SET product_name = ?, price = ?, product_detail = ? WHERE id = ?"
	result, err := r.db.ExecContext(ctx, query, product.Product_Name, product.Price,product.Product_Detail,product.ID)
	if err != nil {
		return 0, err
	}
	updateID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return int(updateID), nil
}


