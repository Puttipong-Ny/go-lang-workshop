package service

import (
	"database/sql"
	//"errors"
	//"net/http"

	//"log"

	"github.com/Peerawitptk/go4web/errs"
	"github.com/Peerawitptk/go4web/logs"
	"github.com/Peerawitptk/go4web/repository"
	"go.uber.org/zap"
	//"golang.org/x/text/unicode/rangetable"
)

type productService struct {
	ProductRepo repository.ProductRepository
}

func NewProductService(ProductRepo repository.ProductRepository) productService{
	return productService{ProductRepo: ProductRepo}
}

func (s productService) GetProducts() ([]ProductResponse, error){
	products, err := s.ProductRepo.GetAll()
	if err != nil{

		logs.Error(err)
		return nil, errs.NewUnexpectedError()
	}
	ProductRespon := []ProductResponse{}
	for _, product := range products {
		ProductResp := ProductResponse{
			ID: product.ID,
			Product_Name: product.Product_Name,
			Price: product.Price,
			Product_Detail: product.Product_Detail,
			Date_Created: product.Date_Created,
			
		}
		ProductRespon = append(ProductRespon, ProductResp)
	} 
	return ProductRespon,nil
}

func (s productService) GetProduct(id int) (*ProductResponse,error) {
	product, err := s.ProductRepo.GetById(id)
	if err != nil{

		if err == sql.ErrNoRows{
			return nil, errs.NewNotFoundError("Customer not found")
		}

		
		logs.Error(err)
		return nil,errs.NewUnexpectedError()
	}
	ProductRespon := ProductResponse{
		ID: product.ID,
		Product_Name: product.Product_Name,
		Price: product.Price,
		Product_Detail: product.Product_Detail,
		Date_Created: product.Date_Created,
	}
	return &ProductRespon, nil
	
}

func(s productService) GetPost(productResponse ProductResponse) (int, error){
	product := repository.Product{
		ID: productResponse.ID,
		Product_Name: productResponse.Product_Name,
		Price: productResponse.Price,
		Product_Detail: productResponse.Product_Detail,
		Date_Created: productResponse.Date_Created,     
    }

	ID, err := s.ProductRepo.GetPost(product)
    if err != nil {
        logs.Error(err)
        return 0, errs.NewValidationError("Validation Error")
    }

	logs.Info("Insert Product  : ",zap.Int("id:", ID))
    return ID, nil

}


func(s productService) GetDeleteById(ID int) error{
	err := s.ProductRepo.GetDeleteById(ID)
	if err != nil {
		logs.Error(err)
		return errs.NewNotFoundError("ID NOT FOUND!")
	}
	logs.Info("DELETE ID  : ", zap.Int("id:", ID))
	return nil
}

func(s productService) GetUpdate(productResp ProductResponse) (int, error){
	product := repository.Product{
		ID: productResp.ID,
        Product_Name: productResp.Product_Name,
        Price: productResp.Price,
        Product_Detail: productResp.Product_Detail,
		Date_Created: productResp.Date_Created,
    }

	ID, err := s.ProductRepo.GetUpdate(product)
    if err != nil {
        logs.Error(err)
        return 0, errs.NewValidationError("Validation Error")
    }
	logs.Info("Update Product  : ", zap.Int("id:", ID))
    return ID, nil
}



