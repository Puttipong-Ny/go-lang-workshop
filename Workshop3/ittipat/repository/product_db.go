package repository

import "github.com/jmoiron/sqlx"

type productRepositoryDB struct {
	db *sqlx.DB
}

func NewProductRepositoryDB(db *sqlx.DB) productRepositoryDB {
	return productRepositoryDB{db: db}
}

func (r productRepositoryDB) GetAll() ([]Product, error) {
	products := []Product{}
	query := "select product_id, product_name, price, product_detail, date_created from product"
	err := r.db.Select(&products, query)
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (r productRepositoryDB) GetById(productID int) (*Product, error) {
	product := Product{}
	query := "select product_id, product_name, price, product_detail, date_created from product where product_id = ?"
	err := r.db.Get(&product, query, productID)
	if err != nil {
		return nil, err
	}
	return &product, nil

}

func (r productRepositoryDB) Create(pro Product) (*Product, error) {
	query := "insert into product (product_id,product_name,price,product_detail,date_created) values (?, ?, ?, ?, ?)"
	result, err := r.db.Exec(
		query,
		pro.ProductID,
		pro.ProductName,
		pro.Price,
		pro.ProductDetail,
		pro.DateCreated,
	)
	if err != nil {
		return nil, err
	}

	proID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}
	pro.ProductID = int(proID)

	return &pro, nil

}

func (r productRepositoryDB) Update(pro Product) (*Product, error) {
	query := "update product set product_name=?, price=?, product_detail=?, date_created=? where product_id=?"
	_, err := r.db.Exec(
		query,
		pro.ProductName,
		pro.Price,
		pro.ProductDetail,
		pro.DateCreated,
		pro.ProductID,
	)
	if err != nil {
		return nil, err
	}

	return &pro, nil
}

func (r productRepositoryDB) Delete(productID int) error {
	query := "DELETE FROM product WHERE product_id = ?"
	_, err := r.db.Exec(query, productID)
	if err != nil {
		return err
	}

	return nil
}
