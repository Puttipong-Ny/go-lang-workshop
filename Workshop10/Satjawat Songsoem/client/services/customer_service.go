package services

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"
)

type Customer struct {
	CustomerID   string
	CustomerName string
	PhoneNumber  string
	BirthDate    string
}

type CustomerService interface {
	GetCustomers() error
	GetCustomer(string) error
	InsertCustomer(Customer) error
	RemoveCustomer(string) error
	UpdateCustomer(Customer) error
}

type customerService struct {
	customerClient CustomerClient
}

func NewCustomerService(customerClient CustomerClient) CustomerService {
	return customerService{customerClient}
}

func (base customerService) GetCustomers() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	req := &Empty{}
	stream, err := base.customerClient.GetCustomers(ctx, req)
	if err != nil {
		log.Printf("Error calling GetCustomers: %v", err.Error())
		return err
	}
	for {
		row, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error receiving customer data: %v", err.Error())
		}
		log.Printf("Customer data: %v", row)
	}
	return nil
}

func (base customerService) GetCustomer(id string) error {

	req := &Id{
		CustomerId: id,
	}
	cust, err := base.customerClient.GetCustomer(context.Background(), req)
	if err != nil {
		log.Printf("Error calling GetCustomer: %v", err.Error())
		return err
	}

	customer := &Customer{
		CustomerID:   cust.CustomerId,
		CustomerName: cust.Name,
		PhoneNumber:  cust.PhoneNumber,
		BirthDate:    cust.BirthDate,
	}

	fmt.Printf("Customer ID %v data: %v", customer.CustomerID, customer)
	return nil
}

func (base customerService) InsertCustomer(customer Customer) error {

	req := &CustomerRecord{
		CustomerId:  customer.CustomerID,
		Name:        customer.CustomerName,
		PhoneNumber: customer.PhoneNumber,
		BirthDate:   customer.BirthDate,
	}

	res, err := base.customerClient.InsertCustomer(context.Background(), req)
	if err != nil {
		log.Printf("Error calling InsertCustomer: %v", err.Error())
		return err
	}
	fmt.Printf("Insert Customer ID: %v",res.CustomerId)
	return nil
}

func (base customerService) RemoveCustomer(id string) error {

	req := &Id{
		CustomerId: id,
	}
	res, err := base.customerClient.DeleteCustomer(context.Background(), req)
	if err != nil {
		log.Printf("Error calling RemoveCustomer: %v", err.Error())
		return err
	}
	fmt.Printf(res.Value)
	return nil
}

func (base customerService) UpdateCustomer(customer Customer) error {

	req := &CustomerRecord{
		CustomerId:  customer.CustomerID,
		Name:        customer.CustomerName,
		PhoneNumber: customer.PhoneNumber,
		BirthDate:   customer.BirthDate,
	}

	res, err := base.customerClient.UpdateCustomer(context.Background(), req)
	if err != nil {
		log.Printf("Error calling UpdateCustomer: %v", err.Error())
		return err
	}
	fmt.Printf(res.Value)
	return nil
}
