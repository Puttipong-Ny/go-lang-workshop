package main

import (
	"client/services"
	"flag"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
)

func main() {

	var cc *grpc.ClientConn
	var err error
	var creds credentials.TransportCredentials

	host := flag.String("host", "localhost:8000", "gRPC server host")
	tls := flag.Bool("tls", false, "use a secure TLS connection")
	flag.Parse()

	if *tls {
		certFile := "../tls/ca.crt"
		creds, err = credentials.NewClientTLSFromFile(certFile, "")
		if err != nil {
			log.Fatal(err)
		}
	} else {
		creds = insecure.NewCredentials()
	}

	cc, err = grpc.Dial(*host, grpc.WithTransportCredentials(creds))
	if err != nil {
		log.Fatal(err)
	}
	defer cc.Close()

	custClient := services.NewCustomerClient(cc)
	custService := services.NewCustomerService(custClient)
	
	/*
	cust := services.Customer{
		CustomerID:   "12345",
		CustomerName: "CCCCCC",
		PhoneNumber:  "0999999999",
		BirthDate:    "15/12/2544",
	}*/
	
	err = custService.GetCustomers()
	//err = custService.GetCustomer("5")
	//err = custService.InsertCustomer(cust)
	//err = custService.UpdateCustomer(cust)
	//err = custService.RemoveCustomer("12345")
	

	if err != nil {
		if grpcErr, ok := status.FromError(err); ok {
			log.Printf("[%v] %v", grpcErr.Code(), grpcErr.Message())
		} else {
			log.Fatal(err)
		}
	}
}