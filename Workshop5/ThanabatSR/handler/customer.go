package handler

import (
	"ThanabatSR/Workshop4/ThanabatSR/service"
	"fmt"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type customerHandler struct {
	custSrv service.CustomerService
}

func NewCustomerHandler(custSrv service.CustomerService) customerHandler {
	return customerHandler{custSrv: custSrv}
}

func (h customerHandler) GetCustomers(c *fiber.Ctx) error {
	customers, err := h.custSrv.GetCustomers()
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	return c.JSON(customers)
}

func (h customerHandler) GetCustomer(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString("Invalid ID")
	}

	customer, err := h.custSrv.GetCustomer(id)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	if customer == nil {
		return c.Status(fiber.StatusNotFound).SendString("Customer not found")
	}

	return c.JSON(customer)
}

func (h customerHandler) DeleteCustomer(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString("Invalid ID")
	}

	err = h.custSrv.DeleteCustomer(id)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}

	return c.SendStatus(fiber.StatusNoContent)
}

func (h customerHandler) InsertCustomer(c *fiber.Ctx) error {
	var req service.CustomerRequest
	if err := c.BodyParser(&req); err != nil {
		return c.Status(fiber.StatusBadRequest).SendString("Invalid request body")
	}

	customer, err := h.custSrv.InsertCustomer(req)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}

	return c.Status(fiber.StatusCreated).JSON(customer)
}

func (h customerHandler) UpdateCustomer(c *fiber.Ctx) error {
	id, err := strconv.Atoi(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).SendString("Invalid ID")
	}

	var req service.CustomerRequest
	if err := c.BodyParser(&req); err != nil {
		return c.Status(fiber.StatusBadRequest).SendString("Invalid request body")
	}

	customer, err := h.custSrv.UpdateCustomer(id, req)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
	}
	if customer == nil {
		return c.Status(fiber.StatusNotFound).SendString("Customer not found")
	}

	return c.JSON(customer)
}
