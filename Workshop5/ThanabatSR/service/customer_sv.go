package service

import (
	"ThanabatSR/Workshop4/ThanabatSR/logs"
	"ThanabatSR/Workshop4/ThanabatSR/repository"
	"strconv"

	"go.uber.org/zap"
)

type customerServiceImpl struct {
	repo repository.CustomerRepository
}

func NewCustomerService(repo repository.CustomerRepository) CustomerService {
	return &customerServiceImpl{repo: repo}
}

func (cs *customerServiceImpl) GetCustomers() ([]CustomerResponse, error) {
	customers, err := cs.repo.GetAll()
	if err != nil {
		logs.Error("Customer not found")
		return nil, err
	}

	var customerResponses []CustomerResponse
	for _, c := range customers {
		customerResponses = append(customerResponses, CustomerResponse{
			CustomerID: c.CustomerID,
			Name:       c.Name,
		})
	}
	logs.Info("All Customers")
	return customerResponses, nil
}

func (cs *customerServiceImpl) InsertCustomer(custReq CustomerRequest) (*CustomerResponse, error) {
	customer := &repository.Customer{
		CustomerID:  custReq.CustomerID,
		Name:        custReq.Name,
		PhoneNumber: custReq.PhoneNumber,
		DateCreate:  custReq.DateCreate,
	}
	newCustomer, err := cs.repo.InsertCustomer(*customer)
	if err != nil {
		logs.Error("Can't insert customer")
		return nil, err
	}
	customerResponse := &CustomerResponse{
		CustomerID: newCustomer.CustomerID,
		Name:       newCustomer.Name,
	}
	custID := strconv.Itoa(custReq.CustomerID)
	logs.Info("Insert Customer", zap.String("ID:",custID))
	return customerResponse, nil
}

func (cs *customerServiceImpl) GetCustomer(id int) (*CustomerResponse, error) {
    customer, err := cs.repo.GetById(id)
    if err != nil {
		logs.Error("Customer not found")
        return nil, err
    }

    customerResponse := &CustomerResponse{
        CustomerID:  customer.CustomerID,
        Name:        customer.Name,
    }
	custID := strconv.Itoa(id)
	logs.Info("Get Customer", zap.String("ID:",custID))
    return customerResponse, nil
}

func (cs *customerServiceImpl) DeleteCustomer(id int) error {
    err := cs.repo.DeleteCustomer(id)
    if err != nil {
		logs.Error("Customer not found")
        return err
    }
	custID := strconv.Itoa(id)
	logs.Info("Delete Customer", zap.String("ID:",custID))
    return nil
}

func (cs *customerServiceImpl) UpdateCustomer(id int, c CustomerRequest) (*CustomerResponse, error) {
    customer := &repository.Customer{
        CustomerID:  c.CustomerID,
        Name:        c.Name,
        PhoneNumber: c.PhoneNumber,
        DateCreate:  c.DateCreate,
    }
    updatedCustomer, err := cs.repo.UpdateCustomer(id, *customer)
    if err != nil {
		logs.Error("Customer not found")
        return nil, err
    }
    customerResponse := &CustomerResponse{
        CustomerID: updatedCustomer.CustomerID,
        Name:       updatedCustomer.Name,
    }
	custID := strconv.Itoa(id)
	logs.Info("Update Customer", zap.String("ID:",custID))
    return customerResponse, nil
}
