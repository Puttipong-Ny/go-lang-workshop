package main

import (
	"context"
	"fmt"
	"gofiber/handler"
	"gofiber/repository"
	"gofiber/service"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var db *gorm.DB

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

type SqlLogger struct {
	logger.Interface
}

func (l SqlLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	sql, _ := fc()
	fmt.Printf("%v\n=============================\n", sql)
}

func SetupDB() *gorm.DB {
	var err error
	initConfig()
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	)
	dial := mysql.Open(dsn)
	db, err := gorm.Open(dial, &gorm.Config{
		Logger: &SqlLogger{},
		DryRun: false})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(db)

	return db
}

func main() {
	db = SetupDB()
	app := fiber.New()

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,PUT,DELETE",
		AllowHeaders: "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token",
	}))

	customerRepositoryDB := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepositoryDB)
	customerHandler := handler.NewCustomerHandler(customerService)

	app.Get("/customer/:customerID", customerHandler.GetCustomer)
	app.Get("/customer", customerHandler.GetCustomers)
	app.Post("/customer", customerHandler.InsertCustomer)
	app.Put("/customer", customerHandler.UpdateCustomer)
	app.Delete("/customer/:customerID", customerHandler.RemoveCustomer)

	app.Listen(":8000")
}
