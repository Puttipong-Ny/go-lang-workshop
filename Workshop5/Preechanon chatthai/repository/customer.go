package repository

type Customer struct {
	Id           int    `db:"id" json:"id"`
	Name         string `db:"name" json:"name"`
	Phone_number string `db:"phone_number" json:"phone_number"`
	Date_created string `db:"date_created" json:"date_created"`
}

type CustomerRepository interface {
	GetAll() ([]Customer, error)
	GetById(int) (*Customer, error)
	UpdateCustomer(Customer, int) (int, error)
	AddCustomer(Customer) (int, error)
	DeleteCustomer(int) error
}
