package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/Akarit2001/workshop3/handler"
	"github.com/Akarit2001/workshop3/logs"
	"github.com/Akarit2001/workshop3/repository"
	"github.com/Akarit2001/workshop3/service"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func middleware(c *fiber.Ctx) error {
	if c.Method() != http.MethodGet && c.Method() != http.MethodDelete {
		log.Println("Request body: " + string(c.Request().Body()))
	}
	err := c.Next()

	// Log response body
	responseBody := c.Response().Body()
	log.Println("Response body: " + string(responseBody))

	return err
}

func main() {
	initTimeZone()
	initConfig()
	db := initDatabase()
	productRepository := repository.NewProductRepositoryDB(db)
	productService := service.NewProductService(productRepository)
	productHandler := handler.NewProductHandler(productService)

	app := fiber.New()
	var urlpath string = "/products/:pid"
	app.Use(middleware)
	app.Use(cors.New())
	app.Use(logger.New(logger.Config{
		TimeZone: "Asia/Bangkok",
	}))
	app.Get("/products", productHandler.GetProducts)
	app.Get(urlpath, productHandler.GetProduct)
	app.Post("/products", productHandler.AddNewProduct)
	app.Put(urlpath, productHandler.UpdateProduct)
	app.Delete(urlpath, productHandler.DeleteProduct)

	logs.Info("service started at port: " + viper.GetString("app.port"))
	app.Listen(fmt.Sprintf(":%v", viper.GetString("app.port")))
}

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

func initTimeZone() {
	ict, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = ict
}

func initDatabase() *gorm.DB {
	// "root:root@tcp(127.0.0.1:3306)/tcc_workshop?parseTime=true"
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.pasword"),
		viper.GetString("db.host"),
		viper.GetString("db.port"),
		viper.GetString("db.databasename"),
	)
	dial := mysql.Open(dsn)
	db, err := gorm.Open(dial)
	if err != nil {
		panic(err)
	}
	// db.SetConnMaxLifetime(3 * time.Minute)
	// db.SetMaxOpenConns(10)
	// db.SetMaxIdleConns(10)
	return db
}
