package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var Db *sql.DB

const customerPath, basePath = "customers", "/api"

type Customer struct {
	CustomerId   int    `json: "customerid"`
	CustomerName string `json: "customername"`
	Phone_number string `json: "phone_number"`
	Date_created string `json: "date_created"`
}

func getCustomerList() ([]Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	rows, err := Db.QueryContext(ctx, "SELECT * FROM customer")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	customer := make([]Customer, 0)
	for rows.Next() {
		var c Customer
		rows.Scan(&c.CustomerId, &c.CustomerName, &c.Phone_number, &c.Date_created)

		customer = append(customer, c)
	}
	return customer, nil
}

func insertCustomer(c Customer) (int, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	rows, err := Db.ExecContext(ctx, `
	INSERT INTO customer (id, name, phone_number, date_created) VALUES (?, ?, ?, ?)`,
		c.CustomerId, c.CustomerName, c.Phone_number, c.Date_created)
	if err != nil {
		log.Panicln(err.Error())
		return 0, err
	}
	insertID, err := rows.LastInsertId()
	if err != nil {
		log.Panicln(err.Error())
		return 0, err
	}
	return int(insertID), nil

}

func getCustomer(customerid int) (*Customer, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	rows := Db.QueryRowContext(ctx, "SELECT * FROM customer WHERE id =?", customerid)
	customer := &Customer{}

	err := rows.Scan(&customer.CustomerId, &customer.CustomerName, &customer.Phone_number, &customer.Date_created)
	if err == sql.ErrNoRows {
		return nil, nil
	} else if err != nil {
		log.Println(err)
		return nil, err
	}
	return customer, nil
}

func deleteCustomer(customerid int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err := Db.ExecContext(ctx, "DELETE FROM customer WHERE id =?", customerid)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

func apiDb() {

	var err error
	Db, err = sql.Open("mysql", "root@tcp(127.0.0.1:3306)/workbench")
	if err != nil {
		panic(err)
	}

	fmt.Println(Db)

	Db.SetConnMaxLifetime(time.Minute * 3)
	Db.SetMaxOpenConns(10)
	Db.SetMaxIdleConns(10)

}

func handleCustomers(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		customerList, err := getCustomerList()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		json_, err := json.Marshal(customerList)
		if err != nil {
			log.Fatal(err)
		}
		_, err = w.Write(json_)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("GET request received at %v", time.Now().Format(time.RFC3339))
		log.Printf("GET JSON: %s", string(json_))

	case http.MethodPost:
		var c Customer
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		log.Printf("POST request received at %v", time.Now().Format(time.RFC3339))
		log.Println(c)
		CustomerID, err := insertCustomer(c)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{id:%d}`, CustomerID)))

	case http.MethodOptions:
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func handleCustomer(w http.ResponseWriter, r *http.Request) {
	urlPathSegments := strings.Split(r.URL.Path, fmt.Sprintf("%s/", customerPath))
	if len(urlPathSegments[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	customerID, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	switch r.Method {
	case http.MethodGet:
		customer, err := getCustomer(customerID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if customer == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		json_, err := json.Marshal(customer)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		_, err = w.Write(json_)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("GET request received at %v", time.Now().Format(time.RFC3339))
		log.Printf("GET JSON: %s", string(json_))

	case http.MethodDelete:
		err := deleteCustomer(customerID)
		if err != nil {
			log.Print(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Printf("DELETE request received at %v", time.Now().Format(time.RFC3339))
		log.Printf("DELETE id: %s", customerID)
	}

}

func corsMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token")
		handler.ServeHTTP(w, r)
	})
}

func setupRoutes(apiPath string) {

	customersHandler := http.HandlerFunc(handleCustomers)
	customerHandler := http.HandlerFunc(handleCustomer)

	http.Handle(fmt.Sprintf("%s/%s", apiPath, customerPath), corsMiddleware(customersHandler))
	http.Handle(fmt.Sprintf("%s/%s/", apiPath, customerPath), corsMiddleware(customerHandler))
}

func main() {
	apiDb()
	setupRoutes(basePath)
	log.Fatal(http.ListenAndServe(":5000", nil))
}
