package main

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var Db *sql.DB

const productPath = "products"
const basePath = "/api"

type Product struct {
	ID            int     `json:"id"`
	ProductName   string  `json:"product_name"`
	Price         float64 `json:"price"`
	ProductDetail string  `json:"product_detail"`
	DateCreated   string  `json:"date_created"`
}

func SetupDB() {
	var err error
	Db, err = sql.Open("mysql", "root:root@tcp(127.0.0.1:3306)/tcc_workshop")
	if err != nil {
		log.Fatal(err)
	}
	Db.SetConnMaxLifetime(time.Minute * 3)
	Db.SetMaxOpenConns(10)
	Db.SetMaxIdleConns(10)
}

func getProductList() ([]Product, error) {
	ctx, cancle := context.WithTimeout(context.Background(), time.Second*3)
	defer cancle()
	query := "SELECT id,product_name, price,product_detail,date_created FROM tcc_workshop.product"
	results, err := Db.QueryContext(ctx, query)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	defer results.Close()
	products := make([]Product, 0)
	for results.Next() {
		var p Product
		err := results.Scan(&p.ID, &p.ProductName, &p.Price, &p.ProductDetail, &p.DateCreated)
		if err != nil {
			log.Println(err)
			return nil, err
		}
		products = append(products, p)

	}
	if err := results.Err(); err != nil {
		log.Println(err)
		return nil, err
	}
	return products, nil
}

func insertProduct(p Product) (int, error) {
	ctx, cancle := context.WithTimeout(context.Background(), time.Second*3)
	defer cancle()
	query := "INSERT INTO `tcc_workshop`.`product` (`id`, `product_name`, `price`, `product_detail`, `date_created`) VALUES (?, ?, ?, ?, ?);"

	results, err := Db.ExecContext(ctx, query, p.ID, p.ProductName, p.Price, p.ProductDetail, time.Now())
	if err != nil {
		log.Println(err)
		return 0, err
	}
	insertId, err := results.LastInsertId()
	if err != nil {
		log.Println(err)
		return 0, err
	}
	return int(insertId), nil
}

func handdlerProducts(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		productList, err := getProductList()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		j, err := json.Marshal(productList)

		if err != nil {
			log.Fatal(err)
		}

		_, err = w.Write(j)
		if err != nil {
			log.Fatal(err)
		}
	case http.MethodPost:
		var p Product
		err := json.NewDecoder(r.Body).Decode(&p)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		pid, err := insertProduct(p)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		w.WriteHeader(http.StatusCreated)
		w.Write([]byte(fmt.Sprintf(`{"id":%d}`, pid)))
		return

	case http.MethodOptions:
		return
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
func handdlerProduct(w http.ResponseWriter, r *http.Request) {
	urlPathSegments := strings.Split(r.URL.Path, fmt.Sprintf("%s/", productPath))

	if len(urlPathSegments[1:]) > 1 {
		w.WriteHeader(http.StatusBadRequest)
	}

	pid, err := strconv.Atoi(urlPathSegments[len(urlPathSegments)-1])

	if err != nil {
		log.Print(err)
		w.WriteHeader(http.StatusNotFound)
	}

	switch r.Method {
	case http.MethodGet:
		product, err := getProduct(pid)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		j, err := json.Marshal(product)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		_, err = w.Write(j)

		if err != nil {
			log.Fatal(err)
		}

	case http.MethodDelete:
		err := removeProduct(pid)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	case http.MethodPut:
		product, err := getProduct(pid)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if product == nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		var updatedProduct Product
		err = json.Unmarshal(body, &updatedProduct)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if updatedProduct.ID != pid {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		err = updateProduct(updatedProduct)
		if err != nil {
			log.Print(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

func removeProduct(pid int) error {
	ctx, cancle := context.WithTimeout(context.Background(), time.Second*3)
	defer cancle()
	query := "DELETE FROM `tcc_workshop`.`product` WHERE (`id` = ?);"

	_, err := Db.ExecContext(ctx, query, pid)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func updateProduct(p Product) error {
	ctx, cancle := context.WithTimeout(context.Background(), time.Second*3)
	defer cancle()
	query := "UPDATE `tcc_workshop`.`product` SET `product_name` = ?, `price` = ?, `product_detail` = ?, `date_created` = ? WHERE (`id` = ?);"

	_, err := Db.ExecContext(ctx, query, p.ProductName, p.Price, p.ProductDetail, p.DateCreated, p.ID)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func getProduct(pid int) (*Product, error) {
	ctx, cancle := context.WithTimeout(context.Background(), time.Second*3)
	defer cancle()
	query := "SELECT id,product_name, price,product_detail,date_created FROM tcc_workshop.product WHERE id = ?"

	row := Db.QueryRowContext(ctx, query, pid)
	p := &Product{}
	err := row.Scan(&p.ID, &p.ProductName, &p.Price, &p.ProductDetail, &p.DateCreated)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		} else {
			log.Println(err)
			return nil, err
		}
	}
	return p, nil
}
func enableCorsMiddlewareHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Log the incoming request
		fmt.Printf("[%s]_%s_%s\n", time.Now().Format(time.RFC3339), r.Method, r.URL.Path)

		w.Header().Add("Access-Control-Allow-Origin", "*")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length")

		// Log request body
		requestBody, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("Error reading request body: %v", err)
		} else if r.Method == http.MethodPost || r.Method == http.MethodPut {
			log.Printf("Request body: %s", requestBody)
			// Replace the request body with a new ReadCloser so it can be read again later
			r.Body = ioutil.NopCloser(bytes.NewBuffer(requestBody))
		}

		// Call the handler and get the response
		recorder := httptest.NewRecorder()
		handler.ServeHTTP(recorder, r)
		resp := recorder.Result()

		// Copy the response back to the original response writer
		for key, values := range resp.Header {
			for _, value := range values {
				w.Header().Add(key, value)
			}
		}
		w.WriteHeader(resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Printf("Error reading response body: %v", err)
		} else {
			log.Printf("Response body: %s", body)
			// Write the response body back to the original response writer
			_, err = w.Write(body)
			if err != nil {
				log.Printf("Error writing response body: %v", err)
			}
		}
	})
}

func setupRoutes(apiBasePath string) {
	productListHandler := http.HandlerFunc(handdlerProducts)
	http.Handle(fmt.Sprintf("%s/%s", apiBasePath, productPath), enableCorsMiddlewareHandler(productListHandler))

	productHandler := http.HandlerFunc(handdlerProduct)
	http.Handle(fmt.Sprintf("%s/%s/", apiBasePath, productPath), enableCorsMiddlewareHandler(productHandler))

}
func main() {
	SetupDB()
	setupRoutes(basePath)
	http.ListenAndServe(":8080", nil)
}
