package service

type CustomerResponse struct {
	CustomerID int    `json:"customer_id"`
	Name       string `json:"name"`
}
type CustomerRequest struct {
	CustomerID  int    `json:"customer_id"`
	Name        string `json:"name"`
	DateCreate  string `json:"date_create"`
	PhoneNumber string `json:"phone_number"`
}
type CustomerService interface {
	GetCustomers() ([]CustomerResponse, error)
	GetCustomer(int) (*CustomerResponse, error)
	DeleteCustomer(id int) error
	InsertCustomer(CustomerRequest) (*CustomerResponse, error)
	UpdateCustomer(id int, c CustomerRequest) (*CustomerResponse, error)
}

