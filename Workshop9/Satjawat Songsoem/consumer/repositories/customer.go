package repositories

import (

	"gorm.io/gorm"
)

type Customer struct {
	CustomerID   string
	CustomerName string
	PhoneNumber  string
	BirthDate    string
}

type CustomerRepository interface {
	//GetCustomers() ([]Customer, error)
	//GetCustomer(int) (*Customer, error)
	InsertCustomer(Customer) error
	RemoveCustomer(string) error
	UpdateCustomer(Customer) error
}

type customerRepository struct {
	db *gorm.DB
}

func NewCustomerRepository(db *gorm.DB) customerRepository {
	return customerRepository{db}
}

/*
func (obj customerRepository) GetCustomers() ([]Customer, error){
	customers := []Customer{}
	result := obj.db.Preload(clause.Associations).Find(&customers)

	if result.Error != nil {
		log.Printf("Error getting all customers: %v", result.Error)
		return nil, result.Error
	}
	log.Printf("Retrieved all customers (%d total)", len(customers))
	return customers, nil
}

func (obj customerRepository) GetCustomer(id int) (*Customer, error) {
	customer := &Customer{}
	err := obj.db.Where("customer_id = ?", id).First(&customer).Error
	if err != nil {
		log.Printf("Error getting customer with ID %d: %v", id, err)
		return nil, err
	}
	log.Printf("Retrieved customer with ID %d", id)
	return customer, nil
}
*/

func (obj customerRepository) InsertCustomer(customer Customer) error {
	return obj.db.Create(&customer).Error
}

func (obj customerRepository) RemoveCustomer(customerID string) error {
	return obj.db.Where("customer_id=?", customerID).Delete(&Customer{}).Error
	//return obj.db.Model(&Customer{}).Where("customer_id = ?",customerID).Delete(&Customer{}).Error
}


func (obj customerRepository) UpdateCustomer(customer Customer) error {
	return obj.db.Model(&Customer{}).Where("customer_id = ?", customer.CustomerID).Updates(customer).Error
}
