package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type customerRepositoryRedis struct {
	db          *gorm.DB
	redisClient *redis.Client
}

func NewCustomerRepositoryRedis(db *gorm.DB, redisClient *redis.Client) CustomerRepository {
	db.AutoMigrate(&Customer{})
	mockData(db)
	return customerRepositoryRedis{db, redisClient}
}

func (r customerRepositoryRedis) GetCustomers() (customers []Customer, err error) {
	time.Sleep(time.Microsecond * 1000)
	key := "repository::GetCustomers"

	// Redis Get
	customersJson, err := r.redisClient.Get(context.Background(), key).Result()
	if err == nil {
		err = json.Unmarshal([]byte(customersJson), &customers)
		if err == nil {
			fmt.Println("redis")
			return customers, nil
		}
	}

	// Database
	err = r.db.Preload(clause.Associations).Find(&customers).Limit(30).Error
	if err != nil {
		return nil, err
	}

	// Redis Set
	data, err := json.Marshal(customers)
	if err != nil {
		return nil, err
	}

	err = r.redisClient.Set(context.Background(), key, string(data), time.Second*10).Err()
	if err != nil {
		return nil, err
	}

	fmt.Println("database")
	return customers, nil
}
