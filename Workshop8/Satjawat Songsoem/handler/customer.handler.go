package handler

import (
	"fmt"
	"gofiber/service"

	
	"github.com/gofiber/fiber/v2"
)

type customerHandler struct {
	custSrv service.CustomerService
}

func NewCustomerHandler(custSrv service.CustomerService) CustomerHandler {
	return customerHandler{custSrv}
}

func (h customerHandler) GetCustomers(c *fiber.Ctx) error {
	
		customers, err := h.custSrv.GetCustomers()
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error: %v", err))
		}
		return c.JSON(customers)
	}
